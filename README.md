# Media Extras

Experimental tweaks and and utilities to help working with Media Suite.

We may plug a few gaps of missing functionality, or extend the UI a little.

The Media project is interminably unstable, so some of these things may stop working,
 OR may become solved within the Media project itself.

## Reset Media Thumbnail Action
  
This provides a new "Reset media thumbnail" 'Action' that can run on 'Media Entity' items.

The action becomes available for use in the Media management view
``/admin/content/media``
and should be available for 'Rules' and other triggered actions also.

This may be eligible for contributing back into media_entity.module, check the status of the issue for progress.

https://www.drupal.org/node/2869239
